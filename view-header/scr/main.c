#include <stdio.h>
#include <stdlib.h>
#include "READER_WRITER.h"
#include "OPEN_CLOSE_FILE.h"
#include "ImageDescription.h"
#include "rotate.h"


int main(int argc, char* argv[]) {
    if (argc < 3){
        fprintf(stderr,"Not enough arguments.\n");
        fprintf(stderr,"Check if you entered file paths.!\n");
    }

    struct image temp_img;
   
    FILE* file_to_read = open_file_toREAD(argv[1]);
    enum read_status temp_read_status = from_bmp(file_to_read, &temp_img);

    if (temp_read_status){
        fprintf(stderr,"reading ERROR\n");
    }
    close_file(file_to_read);

    struct image rotate_img = rotate(temp_img);

    FILE* file_to_write = open_file_toWRITE(argv[2]);
    enum write_status temp_write_status = to_bmp(file_to_write, &rotate_img);

    if (temp_write_status){
        fprintf(stderr,"writing ERROR\n");
    }
    close_file(file_to_write);
    free(temp_img.data);
    free(rotate_img.data);
    return 0;
}